from django.shortcuts import render
#
from django.views.generic import TemplateView, ListView, CreateView
# import models
from .models import Test

class TestView(TemplateView):
    template_name = 'home/test.html'

class TestListView(ListView):
    template_name = "home/list.html"
    context_object_name = 'numbers'
    queryset = "['0', '1', '10', '20, '30']"

class ListTestListView(ListView):
    template_name = "home/list_test.html"
    model = Test
    context_object_name = 'list'

class AddTestCreateView(CreateView):
    template_name = "home/add.html"
    model = Test
    fields = ['title', 'subtitle', 'quantity']