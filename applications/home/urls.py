from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('home/', views.TestView.as_view()),
    path('list/', views.TestListView.as_view()),
    path('list_test/', views.ListTestListView.as_view()),
    path('add/', views.AddTestCreateView.as_view()),
]