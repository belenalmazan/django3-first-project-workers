from django.db import models
from applications.department.models import Department
from ckeditor.fields import RichTextField

# Create your models here.
class Skills(models.Model):
    skill = models.CharField('Skill', max_length=50)

    class Meta:
        verbose_name = 'Skill'
        verbose_name_plural = 'Employee Skills'

    def __str__(self):
        return self.skill

class Employee(models.Model):
    JOB_CHOICES = (
        ('0', 'Accountant'),
        ('1', 'Admin'),
        ('2', 'Economist'),
        ('3', 'Other')
    )
    first_name = models.CharField('First Name', max_length=60)
    last_name = models.CharField('Last Name', max_length=60)
    job = models.CharField('Job', max_length=1, choices=JOB_CHOICES)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to='employee', blank=True, null=True)
    skills = models.ManyToManyField(Skills)
    life_sheet = RichTextField(blank=True)

    class Meta:
        verbose_name = 'Employee'
        verbose_name_plural = 'Company Employees'

    def __str__(self):
        return str(self.id) + '-' + self.last_name + ', ' + self.first_name