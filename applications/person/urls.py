from django.contrib import admin
from django.urls import path

from . import views

app_name = 'person_app'

urlpatterns = [
    path('list_all/', views.ListAllEmployees.as_view()),
    path('list_by_area/<area>/', views.ListByAreaEmployees.as_view()),
    path('search/', views.ListByKwEmployees.as_view()),
    path('skills/', views.ListSkillsEmployees.as_view()),
    path('view/<pk>', views.DetailEmployee.as_view()),
    path('add/', views.EmployeeCreateView.as_view()),
    path('success/', views.SuccessView.as_view(), name='employee_add_success'),
]