from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    TemplateView
)

#models
from .models import Employee

#views
class ListAllEmployees(ListView):
    template_name = "person/list_all.html"
    paginate_by = 4
    ordering = 'first_name'
    model = Employee

class ListByAreaEmployees(ListView):
    template_name = "person/list_all.html"

    def get_queryset(self):
        area = self.kwargs['area']
        list = Employee.objects.filter(
            department__short_name=area
        )
        return list

class ListByKwEmployees(ListView):
    template_name = "person/by_kw.html"
    context_object_name = 'employees'

    def get_queryset(self):
        kword = self.request.GET.get('kword', '')
        list = Employee.objects.filter(
            first_name=kword
        )
        print('lista resultado ==== ', list)
        return list

class ListSkillsEmployees(ListView):
    template_name = "person/skills.html"
    context_object_name = 'skills'

    def get_queryset(self):
        employee = Employee.objects.get(id=1)
        return employee.skills.all()

class DetailEmployee(DetailView):
    model = Employee
    context_object_name = 'employee'
    template_name = 'person/detail.html'

    def get_context_data(self, **kwargs):
        context = super(DetailEmployee, self).get_context_data(**kwargs)
        context['title'] = 'Employee of the month'
        return context

class EmployeeCreateView(CreateView):
    model = Employee
    template_name = "person/add.html"
    fields = ('__all__')
    success_url = reverse_lazy('person_app:employee_add_success')

class SuccessView(TemplateView):
    template_name = "person/success.html"
