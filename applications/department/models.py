from django.db import models

# Create your models here.
class Department(models.Model):
    name = models.CharField('Name', max_length=50, blank=True, editable=False)
    short_name = models.CharField('Short Name', max_length=20, unique=True)
    annulled = models.BooleanField('Annulled', default=False)

    class Meta:
        verbose_name = 'My Department'
        verbose_name_plural = 'Company Departments'
        ordering = ['short_name']
        unique_together = ('name', 'short_name')

    def __str__(self):
        return self.short_name